pipeline {
    agent any

    parameters {
        string(name: 'major_minor_version', defaultValue: '1.0', description: 'The major.minor version for release.')
    }

    options {
        timestamps()
        timeout(time: 15, unit: 'MINUTES') 
        gitLabConnection('gitlab')
    }

    environment {
        AWS_ECR = "644435390668.dkr.ecr.ap-south-1.amazonaws.com"
        RUNTIME_ENV_IP = "13.201.76.155"
        RUNTIME_ENV_USER = "ec2-user@${RUNTIME_ENV_IP}"
        RUNTIME_TEST_PORT = "8085"
    }

    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')    
    }

    stages {

        stage ('Checkout') {
            steps {
                checkout scm
            }
        }


        stage('Build') {
            steps {
                script {
                    try {
                        configFileProvider([configFile(fileId: 'artifactory-settings', variable: 'MAVEN_SETTINGS_XML')]) {
                            sh 'docker build -t toxic-typo-mike .'
                        }
                    } catch (Exception buildError) {
                        echo "Build failed: ${buildError.message}"
                        currentBuild.result = 'FAILURE'
                        error "Build failed."
                    }
                }
            }
        }

        stage('Test') {
            steps {
                script {
                    try {
                        sh '''
                        echo "***********Running Tests" 
                        docker stop toxic-typo-mike || true
                        docker rm toxic-typo-mike || true
                        docker run --publish=8090:8080 -d --name=toxic-typo-mike toxic-typo-mike:latest
                        sleep 5s
                        docker ps
                        docker network ls
                        docker network connect jenkins-setup_ci_net toxic-typo-mike
                        sleep 3
                        docker exec toxic-typo-mike curl -v http://hodesay.servebeer.com:8090
                        docker stop toxic-typo-mike
                        docker rm toxic-typo-mike
                        '''
                    } catch (Exception testError) {
                        echo "Test failed: ${testError.message}"
                        currentBuild.result = 'FAILURE'
                        error "Test failed."
                    }
                }
            }
        }

        stage("Deploy") {
            when {
                branch "main"
            }
            steps {
                script {
                    def deployVersion = 1.0

                    echo "Deploying version: ${deployVersion}"

                    sh("aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin ${AWS_ECR}")
                    sh("docker tag toxic-typo-mike:latest ${AWS_ECR}/toxic-typo-mike:${deployVersion}")
                    sh("docker push ${AWS_ECR}/toxic-typo-mike:${deployVersion}")
                    sh 'docker image rm toxic-typo-mike:latest'
                }
            }
        }  
    }


    post {
        always {
            deleteDir()
        }

        success {
            echo 'Build and deploy successful'
            
        }
        failure {
            echo 'Build and deploy failed'
            
        }

    }
}
